'use strict';
module.exports = {
 up: async (queryInterface, Sequelize) => {
 await queryInterface.createTable('Payments', {
 id: {
 primaryKey: true,
 type: Sequelize.UUID,
 defaultValue: Sequelize.UUIDV4
 },

 payment_id: {
 type: Sequelize.STRING
 },
 user_id: {
 type: Sequelize.STRING
 },
 product: {
 type: Sequelize.STRING
 },
 orderValue: {
 type: Sequelize.STRING
 },
 status: {
 type: Sequelize.STRING
 },

 createdAt: {
 allowNull: false,
 type: Sequelize.DATE
 },
 updatedAt: {
 allowNull: false,
 type: Sequelize.DATE
 }
 });``
 },
 down: async (queryInterface, Sequelize) => {
 await queryInterface.dropTable('Payments');
 }
};