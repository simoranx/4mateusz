'use strict';
import { DataTypes, Model, Optional, UUIDV4 } from 'sequelize';
// These are all the attributes in the Address model
interface PaymentAttributes {
 id: string;
 user_id: string;
 payment_id: string;
 product: string;
 orderValue: string;
 status: string;
 createdAt?: string
 updatedAt?: string
}

// Some attributes are optional in `User.build` and `User.create` calls
interface PaymentCreationAttributes extends Optional<PaymentAttributes, "id"> { }

export class Payments extends Model<PaymentAttributes, PaymentCreationAttributes> implements PaymentAttributes {
  id: string;
  user_id: string;
  payment_id: string;
  product: string;
  orderValue: string;
  status: string;
}

Payments.init({
 id: {
 primaryKey: true,
 type: DataTypes.UUID,
 defaultValue: UUIDV4 // Or Sequelize.UUIDV1
 },
 payment_id: DataTypes.STRING,
 user_id: DataTypes.STRING,
 product: DataTypes.STRING,
 orderValue: DataTypes.STRING,
 status: DataTypes.STRING
 },
 {
 sequelize,
 modelName: 'Payments', tableName: 'Payments'
 });